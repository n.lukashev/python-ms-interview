# Python


## Какие плюсы и минусы Питона ты знаешь?

То что кандидат считает плюсами или минусами. На перечисленных минусах можно остановиться подробнее и попросить предложить способы решения.


## Какие способы ускорения Питон-кода знаешь?

Ожидается разъяснение про приемы с C-либами, Cython, numba, возможно numpy в контексте матричных вычислений. Также рассказ про другие интерпретаторы помимо CPython и JIT-компиляцию


## Работал с генераторами? Как генератор узнает что пора завершить итерации?

Выбрасывает исключение StopIteration.

### Что будет если в генератор поместить return?

При заходе в return генератор сразу же истощится, вернув StopIteration со значением указанным в return.

### Зачем нужны iter и next? Как они вписываются в паттерн "Итератор"?

iter возвращает итератор для итерируемого объекта, next заставляет итератор сделать следующий шаг. Также можно рассказать про соответствующие магические методы.

### Какой еще есть способ создания генератора - без def и без yield?

generator expression: `( val for val in range(10) )`


## Как осуществляется memory management в Питоне?

Через сборщик мусора. Программист напрямую этим не занимается.

### Есть 2 основных способа сборки мусора в Питоне. Каковы они?

Подсчет ссылок и generational garbage collector (модуль gc). Второй способен разрешать циклические ссылки.


## Приходилось иметь дело с тайпхинтами? Как они помогают нам?

Улучшают читаемость кода. Для статичных проверок можно добавить линтеры. IDE также часто тоже способны подсказывать, основываясь на них.

### Что произойдет при подобном вызове:

```Python
def identical_num(x: int) -> int:
    return x

result = identical_num('Vanya')
```

О: никаких ошибок не упадет, функция выполнится до конца, тайпхинты не совершают проверок в рантайме.

### Есть ли в Питоне дженерик-типы?

Можно создать свои при помощи модуля typing: *(даже если кандидат не напишет сам код, это не смертельно)*

```Python
from typing import TypeVar, Generic

T = TypeVar('T')

class MyClass(Generic[T]):
    ...

my_obj = MyClass[str]()
```


## Работал с асинхронностью? На чем основана асинхронность в Питоне?

На верхнем уровне - на корутинах. На нижнем - на ивент лупе uvloop.

### Что такое ивент луп?

Цикл, который выполняет управление асинхронными задачами.

### В каком порядке что выведется в консоль при запуске этого кода?

```Python
import asyncio

async def say_hi():
    print('hi')

async def main():
    say_hi()
    print('bye')
    await asyncio.sleep(0)

asyncio.run(main())
```

О: просто bye, hi не выведется, ошибок не упадет.

**А если заменить 1ю строку main на `await say_hi()`?**

О: сначала hi потом bye.

**А если заменить ее же на `fut = asyncio.ensure_future(say_hi())`?**

О: сначала bye потом hi.

### Что делает ключевое слово async при добавлении к функции/методу?

Превращает его в асинхронную функцию-корутину.

### Напиши свой асинхронный отложенный таймер, который принимает функцию-коллбек и время в секундах, спустя которое коллбек должен вызваться. Коллбек может быть как синхронный так и асинхронный.

```Python
def create_timer(func, sec):
    async def inner():
        await asyncio.sleep(sec)
        if asyncio.iscoroutinefunction(func):
            await func()
        else:
            func()
    return asyncio.create_task(inner())
```

Если таймер будет написан без Task, то стоит попросить написать такой таймер, который можно отменять до окончания исполнения и намекнуть на create_task


## Какие способы конкурентного исполнения питон-кода знаешь?

Мультитрединг, мультипроцессинг, асинхронность.

### В чем разница между тредами и процессами?

Треды живут в пределах процесса и делят общую память. Процессы имеют отдельную память.

### Зачем нужны процессы если есть треды?

Треды не помогают на CPU-bound задачах из-за GIL. (если ранее про это не беседовали то стоит обсудить GIL)

### Как синхронизировать доступ к общему ресурсу (базе данных или питоновскому списку например) в условиях мультитрединга?

С помощью объектов-мьютексов.


## Как разрешается MRO в Питоне? В каком порядке будем искать метод, которого нет в классе A?

```
     A
    /  \
   X    Y
  / \   |
 M   N  U
```

О: Поиск в ширину.


# Базы данных и SQL

## В чем разница между WHERE и HAVING?

Различий много, но основное: WHERE для выбора строк, HAVING для групп столбцов в тандеме с GROUP BY.


## Какие еще есть базы данных кроме реляционных? Применял их?

Ожидается информация про no-sql: документные / key-value / графовые / системы полнотекстного поиска.


## Доводилось дебажить запросы в Postgres? Как это сделать?

Ожидается рассказ про профилировщики EXPLAIN и ANALYZE.


## Что такое индекс?

Ожидается пояснение про ускорение запросов и структуры данных, на которых основаны индексы.

### В таблице person на колонке name висит индекс. Как он повлияет на скорость следующего update-запроса:

```sql
UPDATE person SET co_author_id = 429 WHERE name = 'Leonard C.';
```

О: ускорит

### Может ли такое быть чтобы индекс наоборот - замедлил запрос? В каком случае тот update-запрос был бы наоборот медленее чем без индекса?

Если updat-им саму колонку с индексом и выбираем не по ней.


# Веб

## Что такое политика CORS? Зачем она нужна?

Механизм, необходимый чтобы дать возможность агенту пользователя получать разрешения на доступ к выбранным ресурсам на источнике, отличном от того, что сайт использует в данный момент. Нужно (очевидно) для безопасности.

### Как именно CORS защитит, если я из терминала через curl с левого хоста сделаю запрос на сайт?

Никак. CORS это браузерная политика.

### На основании чего CORS решает, запретить или разрешить доступ к определенному ресурсу?

На основании заголовков Access-Control-Allow-* которые выставляет сервер.


## Как именно HTTPS защищает от уязвимости "man in the middle"?

Это специальное расширение протокола HTTP, которое обеспечивает безопасность передаваемых данных путем шифрования. Без ключей перехваченный трафик не дешифровать.

### Как можно добавить https на свой сайт, который ты поднял на удаленном хосте?

Поставить себе сертификат, который подпишет сторонняя организация. Например, Let's encrypt.


## Мы набрали в адресной строке браузера "google.com" и нажали Enter. Что происходит?

Ожидается более-менее подробный рассказ о пути запросов прежде чем покажут итоговый ресурс. Стоит упомянуть DNS и кеш браузера.


# Архитектура

## Слышал про паттерн Синглтон? Иногда говорят, что это антипаттерн, почему?

* Часто получается так что Синглтон является глобальным объектом, и выходит что разные сервисы мутируют глобальное состояние.
* Наличие синглтона понижает тестируемость приложения.
* Синглтон нарушает Single Responsibility Principle. (минор)


## Какие паттерны использовал в разработке? Про какие знаешь?

Можно рассказывать про что угодно. Интереснее как будет описываться принцип работы или цель применения.


## При проектировании системы обычно стараются добиться высокой связности (cohesion) и низкой связанности (coupling) модулей. Зачем и что это такое?

Связность характеризует отдельно взятый модуль - то насколько модуль является простым с точки зрения его использования. Связанность же - степень независимости модулей. При проектировании систем необходимо стремиться, чтобы модули имели минимальную зависимость друг от друга, что облегчает переиспользование кода и отладку.


## Есть какой-то сервер на fastapi и uvicorn, обрабатывающий входные запросы. Иногда приходит тяжелый таск который отнимает несколько десятков секунд времени и достаточно ресурсов процессора. Как с ним быть?

Ожидается пояснение как минимум про поднятие отдельного процесса (что не особо эффективно с точки зрения ресурсов), а в лучшем случае про отдельного воркера (Celery) и брокера (Redis / Rabbitmq) который передает воркеру таск.


# Теория алгоритмов и структур данных


## Что за структура "куча"? Зачем ее применять? Есть ли в Питоне аналоги?

Дерево, в котором каждый узел-потомок больше или равен родителю (min-куча). Для быстрого поиска минимума/максимума. В питоне модуль heapq.


## Какие алгоритмы сортировок знаешь?

Пусть расскажет что угодно.

### В quicksort и mergesort сложность n x log(n) - откуда там логарифм?

На каждом этапе происходит дробление исходного массива на 2 части, потом их объединяют. Этих дроблений - log по основанию 2.


## Что такое бинарный поиск?

Ожидается пояснение механизма, а также использования на практике.


## Какую структуру будешь использовать для организации очереди FIFO? Как это решить в рамках питона?

Связный список. В питоне есть collections.deque


# Задачи на алгоритмы

## Напиши функцию которая мержит 2 отсортированных списка (просто)

## Напиши стек с методами push и pop, в котором также есть метод get_min, который возвращает минимальное значение в стеке (просто)

## Напиши алгоритм обхода дерева в глубину / ширину (просто)

## Дан массив чисел и число target, выведи пары чисел в массиве, которые в сумме дают этот target (средне)

*Очевидно что в каждом случае надо ожидать наилучших показателей по time и space complexity*
